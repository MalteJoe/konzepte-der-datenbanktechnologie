package de.fhw.dbt.models;

public class Pilot {
    private final String m_name;
    private int          m_points;

    public Pilot(final String name, final int points) {
        this.m_name = name;
        this.m_points = points;
    }

    public void addPoints(final int points) {
        this.m_points += points;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Pilot other = (Pilot) obj;
        if (this.m_name == null) {
            if (other.m_name != null)
                return false;
        } else if (!this.m_name.equals(other.m_name))
            return false;
        if (this.m_points != other.m_points)
            return false;
        return true;
    }

    public String getName() {
        return this.m_name;
    }

    public int getPoints() {
        return this.m_points;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.m_name == null) ? 0 : this.m_name.hashCode());
        result = prime * result + this.m_points;
        return result;
    }

    @Override
    public String toString() {
        return this.m_name + "/" + this.m_points;
    }
}
