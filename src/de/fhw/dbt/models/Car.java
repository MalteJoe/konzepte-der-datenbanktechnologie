package de.fhw.dbt.models;

public class Car {

    private final String model;
    private final Pilot  pilot;

    public Car(final String model) {
        this(model, null);
    }

    public Car(final String model, final Pilot pilot) {
        this.model = model;
        this.pilot = pilot;
    }

    public String getModel() {
        return this.model;
    }

    public Pilot getPilot() {
        return this.pilot;
    }

    @Override
    public String toString() {
        return "Car [model=" + this.model + ", pilot=" + this.pilot + "]";
    }

}
