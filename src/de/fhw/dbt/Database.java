package de.fhw.dbt;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import com.db4o.query.Query;

import de.fhw.dbt.models.Pilot;

/**
 * Wrapper class for work with a DB4O-Database.
 *
 * @author malte
 */
public final class Database implements AutoCloseable {

    /** ObjectContainer representing the database. */
    private ObjectContainer db;
    /** Filename of the storage file for this database. */
    private final String    dbName;

    /**
     * Constructor with the file to use.
     *
     * @param dbName Filename of the database to use
     */
    public Database(final String dbName) {
        this.dbName = dbName;
        open();
    }

    /**
     * Adds points to a pilot's score.
     *
     * @param points the amount of points to add
     * @param pilot the name of the pilot
     */
    public void addPoints(final int points, final String pilot) {
        final List<Pilot> pilots = this.db.queryByExample(new Pilot(pilot, 0));
        if (pilots.size() == 1) {
            final Pilot actualPilot = pilots.get(0);
            actualPilot.addPoints(points);
            this.db.store(actualPilot);
        } else {
            System.err.println("Pilot " + pilot + (pilots.isEmpty() ? " not found!" : " is ambigous!"));
        }
    }

    @Override
    public void close() throws Exception {
        this.db.close();
        this.db = null;

    }

    /**
     * Return a new query Object for SODA-Queries.
     *
     * @return new query Object for SODA-Queries
     */
    public Query createQuery() {
        return this.db.query();
    }

    /**
     * Delete all Objects matching the template.
     *
     * @param template delete objects matching this template
     */
    public void delete(final Object template) {
        this.db.queryByExample(template).forEach(this.db::delete);

    }

    /**
     * Print the contents of the whole database.
     *
     * @return List with all the Objects
     */
    public List<Object> getAll() {
        return getAllObjectsMatching(null);

    }

    /**
     * Return all objects in matching the template.
     *
     * @param <T> type of the objects
     * @param template return objects matching this template
     * @return objects matching the template
     */
    public <T> List<T> getAllObjectsMatching(final T template) {
        return this.db.queryByExample(template);
    }

    /**
     * Return all objects matching the predicate.
     *
     * @param <T> Type of the queried objects.
     * @param predicate predicate for matching object candidates
     * @return objects matching the predicate
     */
    public <T> List<T> getAllObjectsMatchingPredicate(final Predicate<T> predicate) {
        return this.db.query(predicate);
    }

    /**
     * Return all objects in matching the template.
     *
     * @param <T> type of the objects
     * @param template return objects matching this template
     * @return objects matching the template
     */
    public <T> List<T> getAllObjectsMatchingType(final Class<T> template) {
        return this.db.queryByExample(template);
    }

    /**
     * Open the Database Container file.
     */
    public void open() {
        open(Db4oEmbedded.newConfiguration());

    }

    /**
     * Open the Database Container file.
     * 
     * @param config Konfiguration
     */
    public void open(final EmbeddedConfiguration config) {
        assert this.db == null : "Database must be closed first";
        this.db = Db4oEmbedded.openFile(config, this.dbName);

    }

    /**
     * Write objects into the database.
     *
     * @param objects objects to write
     */
    public void store(final Collection<Object> objects) {
        objects.forEach(this.db::store);
    }

    /**
     * Write objects into the database.
     *
     * @param objects objects to write
     */
    public void store(final Object... objects) {
        store(Arrays.asList(objects));
    }

}
