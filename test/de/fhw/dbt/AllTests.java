package de.fhw.dbt;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Ueb01Test.class, Ueb02Test.class, Ueb03Test.class })
public class AllTests {

}
