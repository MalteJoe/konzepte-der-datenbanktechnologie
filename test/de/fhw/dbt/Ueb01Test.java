package de.fhw.dbt;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fhw.dbt.models.Pilot;

public class Ueb01Test {

    /** Filename for the database */
    private static final String   DB_NAME   = "db/" + Ueb01Test.class.getSimpleName() + ".db4o";

    /** Test data for initial filling of the database */
    private static final Object[] TEST_DATA = { new Pilot("Michael Schumacher", 0), new Pilot("Nick Heidfeld", 0),
            new Pilot("Sebastian Vettel", 0), new Pilot("Rubens Barrichello", 0) };

    /** Database access */
    private static Database  db;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final File dbFile = new File(DB_NAME);
        dbFile.mkdirs();
        dbFile.delete();
        db = new Database(DB_NAME);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        db.close();
        new File(DB_NAME).delete();
    }

    @After
    public void cleanUp() throws Exception {
        db.delete(null);
    }

    @Before
    public void setUp() throws Exception {
        // CREATE: Store some values
        db.store(TEST_DATA);
    }

    @Test
    public final void testAddPoints() {
        db.addPoints(12, "Michael Schumacher");
        Assert.assertEquals(12, db.getAllObjectsMatching(new Pilot("Michael Schumacher", 0)).get(0).getPoints());
    }

    @Test
    public final void testCreate() {
        final HashSet<Object> allObjects = new HashSet<>(db.getAll());
        Assert.assertEquals(4, allObjects.size());
        final Collection<Object> c = new HashSet<>();
        Collections.addAll(c, TEST_DATA);
        Assert.assertEquals(c, allObjects);
    }

    @Test
    public final void testDelete() {
        db.delete(new Pilot("Michael Schumacher", 0));
        Assert.assertEquals(3, db.getAll().size());
    }

}
