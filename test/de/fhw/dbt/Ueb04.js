function printResult (result) {
    print(tojson(result));
}

function printQuery (query) {
    print(query);
    query.forEach(printResult);
}

function printCollection (collection) {
    printQuery(collection.find({}));
}

// Löschen der evtl alten Datenbank
db.dropDatabase()

// Anlegen der Daten
db.buch.insert({invNr: 1, autor: "Marc-Uwe Kling", titel: "Die Känguru-Chroniken: Ansichten eines vorlauten Beuteltiers", verlag: "Ullstein-Verlag"});
db.buch.insert({invNr: 2, autor: "Andreas Eschbach", titel: "Ausgebrannt", verlag: "Carlsen"});
db.buch.insert({invNr: 3, autor: "Horst Evers", titel: "Der König von Berlin", verlag: "Rowohlt-Verlag"});

db.leser.insert({lNr: 1, name: "Friedrich Funke", adresse: {straße: "Bahnhofstraße", hausNr: "17", plz: 23758, ort: "Oldenburg"}});
db.leser.insert({lNr: 2, name: "Malte Jörgens", adresse: {straße: "Esinger Weg", hausNr: "43", plz: 25436, ort: "Tornesch"}});
db.leser.insert({lNr: 3, name: "Heinz Müller", adresse: {straße: "Klopstockweg", hausNr: "17", plz: 38124, ort: "Braunschweig"}, entliehen: [1, 3]});

db.entliehen.insert({refLNr: 2, refInvNr: 1, rückgabedatum: new Date(2015, 11, 18)});
db.entliehen.insert({refLNr: 2, refInvNr: 2, rückgabedatum: new Date(2015, 11, 21)})

// Suchen
printQuery(db.buch.find({autor: "Marc-Uwe Kling"}));

print("Anzahl der eingetragenen Bücher: " + db.buch.count());

// Ermitteln Sie bitte alle Leser, die mehr als ein Buch ausgeliehen haben, absteigend sortiert nach Anzahl der entliehenen Bücher
printQuery(db.entliehen.aggregate([{$group: {_id:'$refLNr', anzahlEntliehen:{$sum:1}}},{$sort: {anzahlEntliehen: -1}}])); //TODO Leser anzeigen

// Lassen Sie Friedrich Funke das Känguru-Buch ausleihen und wieder zurückgeben.
printCollection(db.entliehen);
db.entliehen.insert({refLNr: 1, refInvNr: 1, rückgabedatum: new Date(2015, 11, 18)});
printCollection(db.entliehen);
db.entliehen.remove({refLNr: 1, refInvNr: 1});
printCollection(db.entliehen);

// Lassen Sie Heinz Müller das Känguru-Buch zurückgeben. Friedrich Funke soll es wieder ausleihen.
printCollection(db.leser);
db.leser.update({lNr: 3}, {$pull: {entliehen: 1}});
printCollection(db.leser);
db.leser.update({lNr: 1}, {$push: {entliehen: 1}});
printCollection(db.leser);

