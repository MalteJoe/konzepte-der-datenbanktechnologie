package de.fhw.dbt;

import java.io.File;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.db4o.Db4oEmbedded;
import com.db4o.config.EmbeddedConfiguration;

import de.fhw.dbt.models.Car;
import de.fhw.dbt.models.Pilot;

public class Ueb03Test {

    /** Filename for the database */
    private static final String DB_NAME = "db/" + Ueb03Test.class.getSimpleName() + ".db4o";

    /** Database access */
    private static Database     db;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final File dbFile = new File(DB_NAME);
        dbFile.mkdirs();
        dbFile.delete();
        db = new Database(DB_NAME);
        db.close();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        new File(DB_NAME).delete();
    }

    @Rule
    public TestName name = new TestName();

    @After
    public void cleanUp() throws Exception {
        db.delete(null);
        db.close();
    }

    @Before
    public void setUp() throws Exception {
        // CREATE: Store some values
        db.open();
        System.out.println("\n===== " + this.name.getMethodName() + " ======");
        final Pilot schumacher = new Pilot("Michael Schumacher", 0);
        db.store(schumacher, new Car("Ferrari F310", schumacher));
        db.close();
    }

    @Test
    public final void testA() {
        db.open();
        final List<Car> cars = db.getAllObjectsMatchingType(Car.class);
        cars.forEach(System.out::println);
        Assert.assertEquals(1, cars.size());
        Assert.assertNotNull(cars.get(0));
        Assert.assertNotNull(cars.get(0).getPilot());
        Assert.assertNotNull(cars.get(0).getPilot().getName());
    }

    @Test
    public final void testB() throws Exception {
        db.open();
        Car ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        ferrari.getPilot().addPoints(15);
        db.store(ferrari);
        db.close();
        db.open();
        ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        System.out.println(ferrari);
        Assert.assertNotNull(ferrari);
        Assert.assertNotNull(ferrari.getPilot());
        Assert.assertEquals(0, ferrari.getPilot().getPoints());
    }

    @Test
    public final void testC() throws Exception {
        final EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Car.class).cascadeOnUpdate(true);
        db.open(config);
        Car ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        System.out.println(ferrari);
        ferrari.getPilot().addPoints(15);
        db.store(ferrari);
        db.close();
        db.open();
        ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        System.out.println(ferrari);
        Assert.assertNotNull(ferrari);
        Assert.assertNotNull(ferrari.getPilot());
        Assert.assertEquals(15, ferrari.getPilot().getPoints());
    }

    @Test
    public final void testD() throws Exception {
        db.open();
        final Car ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        db.delete(ferrari);
        Assert.assertEquals(1, db.getAllObjectsMatchingType(Pilot.class).size());
        Assert.assertEquals(0, db.getAllObjectsMatchingType(Car.class).size());
    }

    @Test
    public final void testE() throws Exception {
        final EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Car.class).cascadeOnDelete(true);
        db.open(config);
        final Car ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        db.delete(ferrari);
        Assert.assertEquals(0, db.getAllObjectsMatchingType(Pilot.class).size());
        Assert.assertEquals(0, db.getAllObjectsMatchingType(Car.class).size());
    }

    @Test
    public final void testF() throws Exception {
        final EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().objectClass(Car.class).cascadeOnDelete(true);
        db.open(config);
        db.getAll().forEach(System.out::println);
        final Pilot schumacher = db.getAllObjectsMatching(new Pilot("Michael Schumacher", 0)).get(0);
        db.store(new Car("Ferrari F14", schumacher));
        final Car ferrari = db.getAllObjectsMatchingType(Car.class).get(0);
        db.delete(ferrari);
        db.getAll().forEach(System.out::println);
        Assert.assertEquals(0, db.getAllObjectsMatchingType(Pilot.class).size());
        Assert.assertEquals(1, db.getAllObjectsMatchingType(Car.class).size());
    }

}
