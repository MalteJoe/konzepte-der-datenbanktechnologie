package de.fhw.dbt;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.db4o.query.Constraint;
import com.db4o.query.Predicate;
import com.db4o.query.Query;

import de.fhw.dbt.models.Pilot;

public class Ueb02Test {

    /** Filename for the database */
    private static final String   DB_NAME   = "db/" + Ueb02Test.class.getSimpleName() + ".db4o";

    /** Test data for initial filling of the database */
    private static final Object[] TEST_DATA = { new Pilot("Michael Schumacher", 100), new Pilot("Nick Heidfeld", 1),
            new Pilot("Sebastian Vettel", 150), new Pilot("Rubens Barrichello", 80) };

    /** Database access */
    private static Database  db;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final File dbFile = new File(DB_NAME);
        dbFile.mkdirs();
        dbFile.delete();
        db = new Database(DB_NAME);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        db.close();
        new File(DB_NAME).delete();
    }

    @After
    public void cleanUp() throws Exception {
        db.delete(null);
    }

    @Before
    public void setUp() throws Exception {
        // CREATE: Store some values
        db.store(TEST_DATA);
    }

    @Test
    public final void testQuery1() {
        // QUERY: Pilots with 100 points
        final Predicate<Pilot> pred = new Predicate<Pilot>() {

            @Override
            public boolean match(final Pilot candidate) {
                return candidate.getPoints() == 100;
            }
        };
        final List<Pilot> pilots = db.getAllObjectsMatchingPredicate(pred);
        Assert.assertEquals(1, pilots.size());
        Assert.assertEquals(new Pilot("Michael Schumacher", 100), pilots.get(0));
    }

    @Test
    public final void testQuery1SODA() {
        // QUERY: Pilots with 100 points
        final Query q = db.createQuery();
        q.constrain(Pilot.class);
        q.descend("m_points").constrain(100);
        final List<Pilot> pilots = q.execute();
        Assert.assertEquals(1, pilots.size());
        Assert.assertEquals(new Pilot("Michael Schumacher", 100), pilots.get(0));
    }

    @Test
    public final void testQuery2() {
        // QUERY: Pilots with 99-199 points or "Rubens Barrichello"
        final Predicate<Pilot> pred = new Predicate<Pilot>() {

            @Override
            public boolean match(final Pilot candidate) {
                int points;
                return (points = candidate.getPoints()) >= 99 && points <= 199
                        || "Rubens Barrichello".equals(candidate.getName());
            }
        };
        final List<Pilot> pilots = db.getAllObjectsMatchingPredicate(pred);
        Assert.assertEquals(3, pilots.size());
        Assert.assertTrue(pilots.contains(TEST_DATA[0]));
        Assert.assertTrue(pilots.contains(TEST_DATA[2]));
        Assert.assertTrue(pilots.contains(TEST_DATA[3]));
    }

    @Test
    public final void testQuery2SODA() {
        // QUERY: Pilots with 99-199 points or "Rubens Barrichello"
        final Query q = db.createQuery();
        q.constrain(Pilot.class);
        q.descend("m_points").constrain(99).greater().equal()
                .and(q.descend("m_points").constrain(199).smaller().equal())
                .or(q.descend("m_name").constrain("Rubens Barrichello"));
        final List<Pilot> pilots = q.execute();
        Assert.assertEquals(3, pilots.size());
        Assert.assertTrue(pilots.contains(TEST_DATA[0]));
        Assert.assertTrue(pilots.contains(TEST_DATA[2]));
        Assert.assertTrue(pilots.contains(TEST_DATA[3]));
    }

    @Test
    public final void testQuery3() {
        // QUERY: Pilots with specific point count
        final Predicate<Pilot> pred = new Predicate<Pilot>() {
            private final int[] points = { 1, 90, 100 };

            @Override
            public boolean match(final Pilot candidate) {
                return Arrays.binarySearch(this.points, candidate.getPoints()) >= 0;
            }
        };
        final List<Pilot> pilots = db.getAllObjectsMatchingPredicate(pred);
        Assert.assertEquals(2, pilots.size());
        Assert.assertTrue(pilots.contains(TEST_DATA[0]));
        Assert.assertTrue(pilots.contains(TEST_DATA[1]));
    }

    @Test
    public final void testQuery3SODA() {
        final int[] points = { 1, 90, 100 };
        // QUERY: Pilots with specific point count
        final Query q = db.createQuery();
        q.constrain(Pilot.class);
        if (points.length > 0) {
            final Constraint c = q.descend("m_points").constrain(points[0]);
            for (int i = 1; i < points.length; i++) {
                c.or(q.descend("m_points").constrain(points[i]));
            }
        }
        final List<Pilot> pilots = q.execute();
        Assert.assertEquals(2, pilots.size());
        Assert.assertTrue(pilots.contains(TEST_DATA[0]));
        Assert.assertTrue(pilots.contains(TEST_DATA[1]));
    }

}
